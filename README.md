# ☕

This is a coffee script made on a Sunday morning just to to test unicode character support of PHP and other tools.


# Example Usage

```php

require("☕");

$☕ = new ☕();
$☕->espresso()->hot()->make()->get();

```

## Output
```text

           ( (
            ) )
          ........
          |      |]
          \      /
           `----'
         ___________
         `---------'
      
```

# Coffee can be on a plate
```php
$☕->latte()->hot()->withPlate()->make()->get();
```

# You can get some sugar

```php
$☕->latte()->hot()->withPlate()->withSugar()->make()->get();
```

# Coffee can become cold

```php
require("☕");
// private $timeToCold = "5 seconds";

$☕ = new ☕();
$☕->latte()->hot()->withPlate()->make()->get();


sleep(10);
print($☕->isHot() ? "Be careful, this is hot." : "This is not hot!");
print("\n");
$☕->get();
```

# You can rewarm the coffee

```php
$☕->warm();
```